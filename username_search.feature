@username_search
Feature: Username Search

  # Given that these users exist:
    # bjones
    # ajones
    # swifty
    # stewartjo
  # These results should happen:
    # searching username for "ajones" returns 1 hit
    # searching username for "jo" returns 3 hits
    # searching for "alice" returns no hits

  @javascript
  Scenario:
      Given the following "users" exists:
        | firstname | lastname | username |
        | bob | jones | bjones |
        | alice | jones | ajones |
        | tom | swift | tswift |
        | jon | stewart | stewartjo |
      And the following "courses" exists:
        | fullname | shortname | format | numsections
        | ECON 102 ALA SP14: Microeconomic Principles (Vasquez, J) | ECON 102 ALA SP14 | weeks | 1 |
        | ECON 302 AL1 SP14: Inter Microeconomic Theory (Vazquez, J) | ECON 302 AL1 SP14 | weeks | 1 |
        | GEOL 103 ONL/OO SP14: Planet Earth QRII (Herrstrom, E) | GEOL 103 ONL/OO SP14 | weeks | 1 |
        | ENGL 104 AE3 SP14: Intro to Film (Basu, A) | ENGL 104 AE3 SP14 | weeks | 1 |
      And the following "course enrolments" exists:
        | user | course | role |
        | admin | ECON 102 ALA SP14 | student |
      And I log in as "admin"
      # When I follow <targetpage>
      And I follow "Turn editing on"
      Then I should see "Turn editing off"
      And I select "Admin Search" from "bui_addblock"

      Then I fill the moodle form with:
        | realname | ajones |
      And I check "U"
      And I press "Search"
      #And I should see "alice jones"

      And I am on homepage
      Then I fill the moodle form with:
        | realname | jo |
      And I check "U"
      And I press "Search"
      #And I should see "bob jones"
      #And I should see "alice jones"
      #And I should see "jon stewart"
      
      And I am on homepage
      Then I fill the moodle form with:
        | realname | alice |
      And I check "U"
      And I press "Search"
      #Then I should see "No users found"
