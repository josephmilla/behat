@fullname_search
Feature: Full Name Search
  
  # Given that these users exist:
    # Bob Jones
    # Alice Jones
    # Tom Swift
    # Jon Stewart
  # These results should happen:
    # searching for "Jon" in full name returns 3 hits
    # searching for "ALICE" returns 1 hit
    # searching for "Nate" returns no hits
 
  @javascript
  Scenario:
    Given the following "users" exists:
      | first name | surname | username |
      | bob | jones | bjones |
      | alice | jones | ajones |
      | tom | swift | tswift |
      | jon | stewart | jstew |
    And the following "courses" exists:
      | fullname | shortnmae | format | coursediplay | numsections
      | ECON 102 ALA SP14: Microeconomic Principles (Vasquez, J) | ECON 102 ALA SP14 | <courseformat> | <coursedisplay> | 1 |
      | ECON 302 AL1 SP14: Inter Microeconomic Theory (Vazquez, J) | ECON 302 AL1 SP14 | <courseformat> | <coursedisplay> | 1 |
      | GEOL 103 ONL/OO SP14: Planet Earth QRII (Herrstrom, E) | GEOL 103 ONL/OO SP14 | <courseformat> | <coursedisplay> | 1 |
      | ENGL 104 AE3 SP14: Intro to Film (Basu, A) | ENGL 104 AE3 SP14 | <courseformat> | <coursedisplay> | 1 |
    And the following "course enrolments" exists:
      | user | course | role |
      | admin | ECON 102 ALA SP14 | admin |
    And I log in as "admin"
    When I follow <targetpage>
    And I press "Turn editing on"
    Then I should see "Turn editing off"
    And I press "Turn editing off"
    And "Turn editing" "button" should exists
    And I follow "Turn editing on"
    And "Turn editing off" "button" should exists
    And I follow "Turn editing off"
    And I should see "Turn editing on"
    And "Turn editing on" "button" should exists
    And I turn editing mode on
    And I click on "Add..." "link" in the "Add a block" "block"
    And I click on "Admin Search"

    Given I fill the moodle form with:
      | realname | Jon |
    And I press "Search"
    And I should see "bob jones"
    And I should see "alice jones"
    And I should see "jon stewart"

    And I am on homepage
    Given I fill the moodle form with:
      | realname | ALICE |
    And I press "Search"
    And I should see "alice jones"
    
    And I am on homepage
    Given I fill the moodle form with:
      | realname | Nate |
    And I press "Search"
    And I should see ""