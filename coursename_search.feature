@coursename_search
Feature: Course Name Search

  # Given courses with full names and short names:
    # full:ECON 102 ALA SP14: Microeconomic Principles (Vasquez, J) short:ECON 102 ALA SP14
    # full:ECON 302 AL1 SP14: Inter Microeconomic Theory (Vazquez, J) short:ECON 302 AL1 SP14
    # full:GEOL 103 ONL/OO SP14: Planet Earth QRII (Herrstrom, E)  short:GEOL 103 ONL/OO SP14 
    # full:ENGL 104 AE3 SP14: Intro to Film (Basu, A)  short:ENGL 104 AE3 SP14

  # These results should happen
    # searching courses for "Vazquez" returns 2 results
    # searching courses for "engl" returns 1 hit
    # searching courses for "ANTH" returns no hits

  @javascript
  Scenario:
    Given the following "users" exists:
      | firstname | lastname | username |
      | bob | jones | bjones |
      | alice | jones | ajones |
      | tom | swift | tswift |
      | jon | stewart | stewartjo |
    And the following "courses" exists:
      | fullname | shortname | format | numsections
      | ECON 102 ALA SP14: Microeconomic Principles (Vasquez, J) | ECON 102 ALA SP14 | weeks | 1 |
      | ECON 302 AL1 SP14: Inter Microeconomic Theory (Vazquez, J) | ECON 302 AL1 SP14 | weeks | 1 |
      | GEOL 103 ONL/OO SP14: Planet Earth QRII (Herrstrom, E) | GEOL 103 ONL/OO SP14 | weeks | 1 |
      | ENGL 104 AE3 SP14: Intro to Film (Basu, A) | ENGL 104 AE3 SP14 | weeks | 1 |
    And the following "course enrolments" exists:
      | user | course | role |
      | admin | ECON 102 ALA SP14 | student |
    And I log in as "admin"
    And I follow "Turn editing on"
    Then I should see "Turn editing off"
    And I select "Admin Search" from "bui_addblock"

    Then I fill the moodle form with:
      | search | Vazquez |
    And I press "Search"
    #And I should see "ECON 102 ALA SP14: Microeconomic Principles (Vasquez, J)"

    And I am on homepage
    Then  I fill the moodle form with:
      | search | engl |
    And I press "Search"
    #And I should see "ENGL 104 AE3 SP14: Intro to Film (Basu, A)"
    
    And I am on homepage
    Then I fill the moodle form with:
      | search | ANTH |
    And I press "Search"
    #And I should see "No courses were found with the words"
