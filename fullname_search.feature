@fullname_search
Feature: Full Name Search
  
  # Given that these users exist:
    # Bob Jones
    # Alice Jones
    # Tom Swift
    # Jon Stewart
  # These results should happen:
    # searching for "Jon" in full name returns 3 hits
    # searching for "ALICE" returns 1 hit
    # searching for "Nate" returns no hits
 
  @javascript
  Scenario:
    Given the following "users" exists:
      | firstname | lastname | username |
      | bob | jones | bjones |
      | alice | jones | ajones |
      | tom | swift | tswift |
      | jon | stewart | stewartjo |
    And the following "courses" exists:
      | fullname | shortname | format | numsections
      | ECON 102 ALA SP14: Microeconomic Principles (Vasquez, J) | ECON 102 ALA SP14 | weeks | 1 |
      | ECON 302 AL1 SP14: Inter Microeconomic Theory (Vazquez, J) | ECON 302 AL1 SP14 | weeks | 1 |
      | GEOL 103 ONL/OO SP14: Planet Earth QRII (Herrstrom, E) | GEOL 103 ONL/OO SP14 | weeks | 1 |
      | ENGL 104 AE3 SP14: Intro to Film (Basu, A) | ENGL 104 AE3 SP14 | weeks | 1 |
    And the following "course enrolments" exists:
      | user | course | role |
      | admin | ECON 102 ALA SP14 | student |
    And I log in as "admin"
    And I follow "Turn editing on"
    Then I should see "Turn editing off"
    And I select "Admin Search" from "bui_addblock"

    Then I fill the moodle form with:
      | realname | Jon |
    And I check "F"
    And I press "Search"
    #And I should see "bob jones"
    #And I should see "alice jones"
    #And I should see "jon stewart"

    And I am on homepage
    Then I fill the moodle form with:
      | realname | alice |
    And I check "F"
    And I press "Search"
    And I should see "alice jones"
    
    And I am on homepage
    Then I fill the moodle form with:
      | realname | Nate |
    And I check "F"
    And I press "Search"
    And I should see "No users found"
    #And I should see "Acceptance test site"
